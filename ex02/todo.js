
let elems = [];

if (document.cookie) {
	elems = getCookie();
	for (let ind = 0; ind < elems.length; ind++) {
		createElem(atob(elems[ind]));
	}
}

document.getElementById('new').onclick = () => {
	let text = prompt("Input text todo", '');

	if (text) {
		createElem(text);
		elems.push(btoa(text));
		document.cookie = "elems=" + btoa(elems.join('.'));
	}
}

document.getElementById('ft_list').onclick = (event) => {
	if (confirm("Delete?")) {
		let elem = event.target.innerHTML;

		for (let ind = elems.length - 1; ind >= 0; ind--) {
			if (atob(elems[ind]) === elem) {
				elems.splice(ind, 1);
			}
		}
		document.cookie = "elems=" + btoa(elems.join('.'));
		ft_list.removeChild(event.target);
	}
}

function createElem(text) {
	let div  = document.createElement('div');

	div.innerHTML = text;
	ft_list.insertBefore(div, ft_list.firstChild);
}

function getCookie() {
	let res;

	res = document.cookie.split('=');
	res = atob(res[1]).split('.');
	return (res);
}