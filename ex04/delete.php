<?php

$todos = [];

if (($handle = fopen('list.csv', 'r')) !== false) {
	while(($line = fgetcsv($handle, 0, ';')) !== false) {
		$todos[$line[0]] = $line[1];
	}
	fclose($handle);
}

unset($todos[$_POST['id']]);

$handle = fopen('list.csv', 'w');
foreach ($todos as $key => $value) {
	fputcsv($handle, [$key, $value], ';');
}
fclose($handle);

return ("OK");

?>