<?php

$todos = [];

if (($handle = fopen('list.csv', 'r')) !== false) {
	while(($line = fgetcsv($handle, 0, ';')) !== false) {
		$todos[$line[0]] = $line[1];
	}
	fclose($handle);
}

echo json_encode($todos);

?>