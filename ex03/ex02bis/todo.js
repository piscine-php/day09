
let elems = [];

if (document.cookie) {
	elems = getCookie();
	for (let ind = 0; ind < elems.length; ind++) {
		createElem(atob(elems[ind]).replace(new RegExp('-', 'g'), '='));
	}
}

$('#new').on('click', function() {
	let text = prompt("Input text todo", '');

	if (text) {
		createElem(text);
		elems.push(btoa(text).replace(new RegExp('=', 'g'), '-'));
		document.cookie = "elems=" + btoa(elems.join('.')).replace(new RegExp('=', 'g'), '-');
	}
});

$('#ft_list').on('click', function(event) {
	if (confirm("Delete?")) {
		let elem = event.target.innerHTML;

		for (let ind = elems.length - 1; ind >= 0; ind--) {
			if (atob(elems[ind].replace(new RegExp('-', 'g'), '=')) === elem) {
				elems.splice(ind, 1);
			}
		}
		document.cookie = "elems=" + btoa(elems.join('.')).replace(new RegExp('=', 'g'), '-');
		$('#ft_list').find(event.target).remove();
	}
});

function createElem(text) {
	$('#ft_list').prepend('<div>' + text + '</div>');
}

function getCookie() {
	let res;

	res = document.cookie.split('=');
	res = atob(res[1].replace(new RegExp('-', 'g'), '=')).split('.');
	
	return (res);
}